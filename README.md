# CCF Anonymization Script #

This repository contains the minimal Connectome Coordination Facility recommended anonymization script.  This script should be considered as a starting point for anonymization.  Data contributed to CCF should be reviewed for protected health information (PHI), to see if their site might include PHI in additional fields, in which case the these
should be included in the local anonymization script.

### Download ###

[ CCF Anonymization Script V1.0 ![Download](https://api.bintray.com/packages/nrgxnat/xnat-plugins/XSync/images/download.svg) ](https://bitbucket.org/hcp/ccf-anonymization-scripts/downloads/ccf_anonymization_script_V1.das)

### Getting set up ###

This page contains information for configuring DICOM anonymization on your local XNAT site:

[Anonymization in XNAT](https://wiki.xnat.org/display/XNAT16/Anonymization)

If you're using XSync to send images, anonymization can be in XSync for the data it sends.  Please see:

[XSync Repository](https://bitbucket.org/xnatdev/xsync)


### Getting Help ###

issues@humanconnectome.org